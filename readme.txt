=== Pokeblock ===
Contributors:      Dan Jones
Tags:              block
Tested up to:      5.8.0
Stable tag:        0.1.0
License:           MIT
License URI:       https://mit-license.org/

Interactive Pokédex which can be embedded on any page.

== Description ==

This plugin provides a Gutenberg block that can be used to embed a Pokédex on any WordPress page/post.

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/pokeblock` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Go to any post or page, and add the Pokéblock block to your page.

== Frequently Asked Questions ==

= Where does this data come from? =

The information displayed is freely provided by the [PokéAPI](https://pokeapi.co/).

= I have other questions about this data. =

Refer to the [About Page](https://pokeapi.co/about) for the API.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 0.1.0 =
* Release

== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above. This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation." Arbitrary sections will be shown below the built-in sections outlined above.
